# RsmlReader

A Plugin to import and export Root system Modeling Language([RSML](http://rootsystemml.github.io/)) files to GroIMP

RSML is a xml based standard to represent root architecture,  for simulation as well as measuring. 

### Installation

This repository holds compiled releases for the java 8 and java 11 version of GroIMP at [Releases · GroIMP plugins / RSMLReader · GitLab](https://gitlab.com/groimp-plugins/RSMLReader/-/releases). The zip direcotrys can be unpacked and added to the plugin folder of your GroIMP instance.

For usage in Eclipse the repository can be cloned or downloaded and imported as an existing eclipse project, **If you are using java 8 the file `/src/module-info.java` must be deleted**

### Usage

#### Import

A .rsml file can either be opened as a new project with `/file/open`, or added to an existing project  via `/Objects/Insert File`.  The imported root system shall be maintaining the original taxonomy and can be edited with the normal GroIMP tools.

#### Export

The export function can export root systems based on the F node to rsml Files, by `/view/export` in the 3dView panel. If no Node is selected the function starts at the root of the project graph and exports the howl model, otherwise only the sub-graph below the selected node will be exported.
