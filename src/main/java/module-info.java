/**
 * 
 */
/**
 * @author tim
 *
 */
module rsmlReader {
	requires platform.core;
	requires platform;
	requires platform.swing;
	requires graph;
	requires xl.core;
	requires math;
	requires vecmath;
	requires rgg;
	requires imp3d;
	requires java.xml;
	requires java.desktop;
	requires utilities;
	requires xl.impl;
	requires xl;
}