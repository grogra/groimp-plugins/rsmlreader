package de.grogra.rsmlReader;

import java.util.ArrayList;


import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

import org.xml.sax.helpers.DefaultHandler;

public class XContentHandler  extends DefaultHandler {
	private ArrayList<Root> allRoots = new ArrayList<Root>();
	private String currentValue;
	int rootid=-1;
	int currentId=-1;
	boolean isGeometry=false;
	boolean isDiameter=false;
	boolean diameterFlag=false;
	Root last;
	
	//getter
	public ArrayList<Root> getAllRoots(){
			return allRoots;
	}
	
//implementing ContentHandler
	public void setDocumentLocator(Locator locator) {
		// TODO Auto-generated method stub
		
	}

	public void startDocument() throws SAXException {
		// TODO Auto-generated method stub
		
	}

	public void endDocument() throws SAXException {
		// TODO Auto-generated method stub
		
	}

	public void startPrefixMapping(String prefix, String uri)
			throws SAXException {
		// TODO Auto-generated method stub
		
	}

	public void endPrefixMapping(String prefix) throws SAXException {
		// TODO Auto-generated method stub
		
	}

	public void startElement(String uri, String localName, String qName,
			Attributes atts) throws SAXException {
		if (localName.equals("root")) {		//starts parsing Roots			
			Root r = new Root(currentId);
			if(currentId!=-1){
				allRoots.get(currentId);
			}
			last = r;
			allRoots.add(r);
			currentId=allRoots.size()-1;
			
			allRoots.get(currentId).setRootId(currentId);
		}
		if(localName.equals("geometry")) {
			isGeometry=true;
		}
		if(localName.equals("function")) {
			if(atts.getLength()>=1 && atts.getValue("name")!=null && atts.getValue("name").equals("diameter")) {
				isDiameter=true;
			}
		}
		if(isDiameter&&localName.equals("sample")) {
			if(atts.getLength()>=1 && atts.getValue("value")!=null) {
				allRoots.get(currentId).addDiameter(Float.parseFloat(atts.getValue("value")));
				diameterFlag=true;
			} 
		}
		
		if(localName.equals("point") && isGeometry) {
			float x= Float.parseFloat(atts.getValue("x"));
			float y= Float.parseFloat(atts.getValue("y"));
			float z=0;
			if(atts.getLength()>2) {
				z= Float.parseFloat(atts.getValue("z"));
			}
			//System.out.println(x+","+y+","+z);
			allRoots.get(currentId).addPoint(x,y,z);

		}
	}

	public void endElement(String uri, String localName, String qName) throws SAXException {
		if(localName.equals("geometry")) {
			isGeometry=false;
		}
		if(localName.equals("function")) {
			isDiameter=false;
		}
			if (localName.equals("root")) {
				currentId=allRoots.get(currentId).getParendId();
			
			}
			if(isDiameter&&localName.equals("sample")) {
				if(!diameterFlag) {
					allRoots.get(currentId).addDiameter(Float.parseFloat(currentValue));
					diameterFlag=false;
				}
			}
		}
	

	public void characters(char[] ch, int start, int length)
			throws SAXException {
		 currentValue = new String(ch, start, length);
		
	}

	public void ignorableWhitespace(char[] ch, int start, int length)
			throws SAXException {
		// TODO Auto-generated method stub
		
	}

	public void processingInstruction(String target, String data)
			throws SAXException {
		// TODO Auto-generated method stub
		
	}

	public void skippedEntity(String name) throws SAXException {
		// TODO Auto-generated method stub
		
	}
}
