package de.grogra.rsmlReader;

import java.util.ArrayList;

import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.math.TMatrix4d;
import de.grogra.turtle.F;
import de.grogra.turtle.Translate;
import de.grogra.vecmath.Math2;

public class Root {
	ArrayList<Point> points;

	int rootId;
	int parendId;
	int diameterId;
	public Root(int pid) {
		parendId=pid;
		points=new ArrayList<Point>();
		diameterId=0;
	}
	public void addDiameter(float d) {
		points.get(diameterId).setDiameter(d);
		diameterId++;
	}
	public TMatrix4d setDirection(Vector3d direction)
    {
        final Matrix3d m = new Matrix3d();
        Math2.getOrthogonalBasis(direction, m, true);
        TMatrix4d tx =new TMatrix4d();
        tx.setRotationScale(m);
        return tx;
    }	
	public void setRootId(int rootId) {
		this.rootId = rootId;
	}
	public void addPoint(float x, float y,float z) {
		points.add(new Point(x,y,z));
	}
	public double getDistance(Point p, Point pn) {
		
		return Math.sqrt(Math.pow(p.getX()-pn.getX(),2)+Math.pow(p.getY()-pn.getY(),2)+Math.pow(p.getZ()-pn.getZ(),2));
	}
	
	public int getParendId() {
		return parendId;
	}

	public Node createGraph(Node root, ArrayList<Root> allRoots, float offSetx,float offSety,float offSetz) {
		Point p = points.get(0);
		Translate t = new Translate();
		t.translateX=(float) p.getX()-offSetx;
		t.translateY=(float) p.getY()-offSety;
		t.translateZ=(float) p.getZ()-offSetz;
		root.addEdgeBitsTo(t,Graph.BRANCH_EDGE, null);
		Node old_node =t;
		for(int i = 0; i<points.size()-1;i++) {
			old_node = points.get(i+1).addToGraph(points.get(i), old_node, allRoots);
		}
		return old_node;
	}
}

class Point{
	ArrayList<Integer> childs;
	float x,y,z;
	Vector3d start;
	float d=(float) 0.1;
	public Point(float x,float y,float z) {
		this.start=new Vector3d(x,y,z);
		childs = new ArrayList<Integer>();
	
	}
	void setDiameter(float d) {
		this.d=d;
	}
	public double getX() {
		return start.x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public double getY() {
		return start.y;
	}
	public double getZ() {
		return start.z;
	}
	public float getD() {
		return d;
	}
	public void addChild(int kiddoId) {
		childs.add(kiddoId);
	}

	public Node addToGraph(Point old, Node n, ArrayList<Root> allRoots) {
		F neu = new F();
		neu.length=(float) getDistance(old,this);
		neu.diameter=old.getD();
		neu.setTransform(new TMatrix4d());
		Matrix3d m = new Matrix3d();
	    Math2.getOrthogonalBasis(new Vector3d(start.x-old.start.x,start.y-old.start.y,start.z-old.start.z), m, true);
	    ((TMatrix4d)neu.getTransform()).setRotationScale(m);
	    RsmlN xy = new RsmlN();
		xy.setTransform(new TMatrix4d());
		((TMatrix4d)xy.getTransform()).invert((Matrix4d) neu.getTransform());
		n.addEdgeBitsTo(neu, Graph.SUCCESSOR_EDGE, null);
		neu.addEdgeBitsTo(xy, Graph.SUCCESSOR_EDGE, null);
		if(this.childs.size()>0){
			for(int c : childs) {
				Node cn =new Node();
				xy.addEdgeBitsTo(cn,Graph.BRANCH_EDGE, null);
				allRoots.get(c).createGraph(cn,allRoots,(float)getX(),(float)getY(),(float)getZ());
			}
			
		}
		return xy;
	}
	public double getDistance(Point p, Point pn) {
		double len=Math.sqrt(Math.pow(p.getX()-pn.getX(),2)+Math.pow(p.getY()-pn.getY(),2)+Math.pow(p.getZ()-pn.getZ(),2));
		return len;
	}
}