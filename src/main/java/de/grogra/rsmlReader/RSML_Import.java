package de.grogra.rsmlReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;


import de.grogra.graph.impl.Node;

public class RSML_Import {
	public RSML_Import(File f, Node root) {
		XMLReader xmlReader = null;
		try {
			xmlReader = XMLReaderFactory.createXMLReader();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    FileReader reader = null;
		try {
			reader = new FileReader(f.getAbsolutePath());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    InputSource inputSource = new InputSource(reader);
	    XContentHandler xc=new XContentHandler();
	    xmlReader.setContentHandler(xc);
	    try {
			xmlReader.parse(inputSource);
		} catch (IOException | SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    for(int i =0; i< xc.getAllRoots().size(); i++) {
	    	Root part = xc.getAllRoots().get(i);
	    	if(part.parendId!=-1) {
	    		getParentPoint(xc.getAllRoots().get(part.parendId),i,part.points.get(0).getX(),part.points.get(0).getY(),part.points.get(0).getZ());
	    	}
	    }
	    
	    
	    Node old = root;
	    for(Root part : xc.getAllRoots()) {
	    	if(part.parendId==-1) {
	    			part.createGraph(old,xc.getAllRoots(),0,0,0);
	    	}
	    }
	}
	double getDistance(double startx, double starty, double startz, double d, double e, double f) {
		
		return Math.sqrt(Math.sqrt(Math.pow(startx-d,2)+Math.pow(startx-d,2))+Math.pow(startx-d,2));
	}
	
	void getParentPoint(Root parent, int kiddoId, double startx,double starty,double startz) {
		System.out.println(startx+","+starty+","+startz);
		int minp=1;
		double tmp=0;
	/*	tmp= Math.abs(parent.points.get(0).getX()-startx);
		tmp+= Math.abs(parent.points.get(0).getY()-starty);
		tmp+= Math.abs(parent.points.get(0).getZ()-startz);
		*/
		tmp=getDistance(startx, starty, startz,parent.points.get(0).getX(),parent.points.get(0).getY(),parent.points.get(0).getZ());
		double min=tmp;
		for(int i=1; i<parent.points.size()-1;i++) {
			System.out.println(tmp);
			tmp=getDistance(startx, starty, startz,parent.points.get(i).getX(),parent.points.get(i).getY(),parent.points.get(i).getZ());		
			
			if(tmp<min) {
				minp=i;
				min=tmp;
			}
		}
		Point x=parent.points.get(minp);
		x.addChild(kiddoId);
		parent.points.set(minp, x);
	
	}
}
